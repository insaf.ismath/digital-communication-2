function [code] = makecode1(prob, s)

  if (length(prob) > 2)

    prob = sort(prob);
    prob(2) = prob(2) + prob(1);
    prob(1) = [];    
    
    s = s(i) ;
    s{2} = {s{1},s{2}};
    s(1) = [];
    makecode1(prob,s)
    
  else
    prob = sort(prob);
    code = 1;
   
  end
  
end


