function [ encoded ] = huff_enc( to_encode )
    
%     p = [8.16, 1.492, 2.782, 4.253, 13.0001, 2.228, 2.015, 6.094, 6.966, 0.153, 0.778, 4.025, 2.406, 6.749, 7.507, 1.929, 0.095, 5.987, 6.327, 9.056, 2.758, 0.978, 2.360, 0.150, 1.974, 0.074];
p = [0.1, 0.1, 0.05, 0.05, 0.2, 0.1, 0.05, 0.05, 0.05, 0.25];
    
s = cell(length(p),1);
    alphabet = Factorial2(p,s);
   
    L = strlength(to_encode);
            
    fileID = fopen('alphabet.t','w');
    
    entropy = 0;
    expected_len = 0;
    sum = 0;
    for x = 1:length(p)      
        sum = sum + p(x);
    end
    
    for x = 1:length(p)      
%         disp(p(x));
%         disp(alphabet{x});
        entropy = entropy + p(x)/sum * log2(sum/p(x));
        expected_len = expected_len +  p(x) * length(alphabet{x}) / sum ;

    end
     
    disp(entropy);
    disp(expected_len);
         
    en = [];
    
%     encoding = cell(L,1);
    
    for i = 1:L
        character = to_encode(i);
        if strcmp(character,' ')
            continue
            
        elseif strcmp(character,',')
            continue
        else
%             int16(character)-96
            en = [en , alphabet{int16(character)-96}];
            
        end
        
        
        
    end
    
    encoded = en;

end

