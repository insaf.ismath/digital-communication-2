function [Result] = make_code( prob, signal )
    if length(prob) > 2
        
        [prob, i]  = sort(prob);
        prob(2) = prob(1) + prob(2);
        prob(1) = []; 
        signal(1) = [];
        
        
        temp = Factorial2( prob, signal );
        add = temp{1};
        temp(1) =[];
        signal = [[add,0], [add,1], temp];
        
        c = cell(length(i));

        for x = 1:length(i);
            c(i(x)) = signal(x);
        end
        
        for x = 1:length(i);
             signal(x) = {c(x)};
        end
%         
        
        signal = c;
        Result = signal;
        
    else
        [prob, i]  = sort(prob);
        
        signal{1} = 0;
        signal{2} = 1;
        
        c = cell(length(i));

        for x = 1:length(i);
            c(i(x)) = signal(x);
        end
        
        for x = 1:length(i);
             signal(x) = {c(x)};
        end
         
        Result = signal;
        

    end
end