clear;

seq = [4 1 5 5 10 2 10 6 7 2 3 10 1 5 6 5 8 10 10];
symbols = [1 2 3 4 5 6 7 8 9 10];
p = [0.1, 0.1, 0.05, 0.05, 0.2, 0.1, 0.05, 0.05, 0.05, 0.25];
tic
dict = huffmandict(symbols,p);
for i = 1: 1000

hcode = huffmanenco(seq,dict);
end
toc
tic
for i = 1:1000
dhsig = huffmandeco(hcode,dict);
end
toc
isequal(seq,dhsig)

disp(length(hcode));
fileID = fopen('encoded_huf.txt','w');
fprintf(fileID,'%d', hcode);
