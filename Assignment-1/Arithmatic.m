clear;



% seq = repmat([4 1 5 5 10 2 10 6 7 2 3 10 1 5 6 5 8 10 10],1,50);
seq = [4 1 5 5 10 2 10 6 7 2 3 10 1 5 6 5 8 10 10];
counts = [10, 10, 5, 5, 20, 10, 5, 5, 5, 25];

tic
for i = 1:1000
code = arithenco(seq,counts);
end
toc
tic
for i = 1:1000
dseq = arithdeco(code,counts,length(seq));
end
toc

disp(length(code));
fileID = fopen('encoded_arith.txt','w');
fprintf(fileID,'%d', code);