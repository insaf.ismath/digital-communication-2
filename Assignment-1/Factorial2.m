function [ Result ] = Factorial2( prob, signal )
  
    if length(prob) > 2
        
        [prob, i]  = sort(prob);
        prob(2) = prob(1) + prob(2);
        prob(1) = []; 
        signal(1) = [];
        
        
        temp = Factorial2( prob, signal );
        add = temp{1};
        temp(1) =[];
        signal = [[add, 1], [add, 0], temp];
        
        c = cell(length(i));

        for x = 1:length(i);
            c(i(x)) = signal(x);
        end
        
        signal = c;
 
        
        Result = signal;
        
    else
        
        Result = {1, 0};
        
    end
end